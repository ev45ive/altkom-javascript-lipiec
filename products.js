
const products = [
  {
    id: '123',
    name: 'Placki "Choco\'s" ',
    description: "Czekolawowe '100g' ",
    price: 10.99,
    promotion: true,
    category: ['Placki'],
    review: { rating: 3 }
  }, {
    id: '234',
    name: 'Placki "Banana\'s" ',
    description: "Bananowe '100g' ",
    price: 12.99,
    promotion: false,
    category: ['Placki'],
    review: { rating: 3 }
  }, {
    id: '345',
    name: 'Naleśniki',
    // description: null,
    price: 6.99,
    promotion: true,
    discount: 0.15,
    category: ['Pancakesy'],
    review: { rating: 3 }
  }
]


// Arguments, defaults
function listProducts(per_page = 3, only_promoted = null) {
  // local variables
  var displayed = 0

  productsElem.innerHTML = '';

  for (let product of products) {

    // Only true or false, ignore null / undefined
    if (only_promoted === product.promotion) {
      continue;
    }

    if (displayed >= per_page) break
    else displayed++


    // Description
    renderProduct(product);
  }
}


function renderProduct(product) {
  const price = calculateDiscount(product);
  const brutto = calculateBruttoPrice(price);
  const category = getCategoryLabel(product);

  const elem = productTpl.content.cloneNode(true)

  elem.firstElementChild.setAttribute('data-product-id', product.id)

  elem.querySelector('[data-bind-title]').innerText = `${product.name} ${product.promotion ? 'PROMO' : ''}`
  elem.querySelector('[data-bind-price]').innerText = brutto
  elem.querySelector('[data-bind-description]').innerText = (product.description ? `${product.description.slice(0, 10)}...` : '')

  elem.querySelector('.js-add-to-cart').onclick = function (event) {
    const parent = event.target.closest('[data-product-id]')
    const productId = parent.dataset.productId
    const product = products.find(p => p.id === productId)

    window.cart.add(product)

    // for(let prod of products){
    //   if(prod.id === productId){
    //     var selected = prod;
    //     break;
    //   }
    // }
  }

  productsElem.append(elem)
}

function getCategoryLabel(product) {
  let category = 'Pozostałe';

  switch (product.category[0]) {
    case 'Placki':
      category = 'Świetne Placki';
      break;
    case 'Pancakesy':
    case 'Naleśniki':
      category = 'Pyszne Naleśniki';
      break;
  }
  return category;
}

function calculateDiscount(product) {
  let price = product.price;

  if (product.promotion && product.discount === undefined) {
    price = product.price * (1 - globalDiscount);

  } else if (product.discount > 0) {
    price = product.price * (1 - product.discount);

  }
  return price;
}

function calculateBruttoPrice(price) {
  return (price * (100 + tax) / 100).toFixed(2);
}