```js
function WhatIsThis(){
    console.log(this) 
}    

WhatIsThis() 
// VM12116:2 Window {window: Window, self: Window, document: document, name: "", location: Location, …}

window.WhatIsThis 
// ƒ WhatIsThis(){
//     console.log(this) 
// }

x = 1
// 1

window.x 
// 1


[1,2,3].reduce(WhatIsThis)
// VM12116:2 Window {window: Window, self: Window, document: document, name: "", location: Location, …}


obj = {} 
// {}
obj.fn = WhatIsThis
// ƒ WhatIsThis(){
//     console.log(this) 
// }
obj.fn()
// {fn: ƒ}

```

## Changing this

```js

WhatIsThis.apply({ x: 1})
// VM12116:2 {x: 1}

WhatIsThis.call({ x: 1})
// VM12116:2 {x: 1}

fn = WhatIsThis.bind({x:2})
fn()
// VM12116:2 {x: 2}

obj.fn2 = fn
obj.fn2()
// VM12116:2 {x: 2}

new WhatIsThis()
// WhatIsThis {}
  // [[Prototype]]: Object
    // constructor: ƒ WhatIsThis()

alwaysWindow = () => WhatIsThis() 
obj.fn = alwaysWindow
obj.fn()
// Window {window: Window, self: Window, document: document, name: "", location: Location, …}


CantConstructThis = () => {}
new CantConstructThis()
// VM13662:2 Uncaught TypeError: CantConstructThis is not a constructor
//     at <anonymous>:2:1


btn = document.createElement('button')
btn.onclick = function(){ console.log(this) }
btn.click()
// VM14169:2 <button>​</button>​


this 
// Window {window: Window, self: Window, document: document, name: "", location: Location, …}
btn = document.createElement('button')
btn.onclick = () => { console.log(this) }
btn.click()
// VM14234:2 Window {window: Window, self: Window, document: document, name: "", location: Location, …}
```