

(function Cart(module = {}) {

  const cart = {
    items: [
      {
        id: '123',
        amount: 2,
        total: 200,
        product: products[0]
      }
    ],
    info: {
      total: 0
    }
  }


  function getCartItems() {
    return cart.items
  }

  function loadCartItems(items) {
    cart.items = items
  }


  function addToCart(product) {
    const item = findCartItem(product.id)

    if (item) {
      item.amount += 1
      item.total = item.amount * item.product.price
    } else {
      cart.items.push({
        id: product.id,
        amount: 1,
        total: product.price,
        product: product
      })
    }
  }

  function removeFromCart(product) {
    const item = findCartItem(product.id)

    if (!item) return


    if (item.amount <= 1) {
      cart.items = cart.items.filter(i => i.id !== item.id)
    } else {
      item.amount--
      item.total = item.amount * item.product.price
    }
  }

  function findCartItem(id) {
    return cart.items.find(item => item.id === id)
  }


  // window.getCartItems = getCartItems
  // window.loadCartItems = loadCartItems
  // window.addToCart = addToCart
  // window.removeFromCart = removeFromCart
  // window.findCartItem = findCartItem

  Object.assign(module, {
    // getCartItems: getCartItems,
    getCartItems,
    loadCartItems,
    addToCart,
    removeFromCart,
    findCartItem,
  })

})(window);

// Cart()