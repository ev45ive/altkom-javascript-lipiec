
const credit_amount = document.querySelector('#credit_amount')
const credit_instalments = document.querySelector('#credit_instalments')
const calculateBtn = document.querySelector('#calculateBtn')
const amountOutput = document.querySelector('#output-amount')
const installmentsOutput = document.querySelector('#output-instalments')
const interestOutput = document.querySelector('#output-interest')
const totalOutput = document.querySelector('#output-total')

credit_amount.value = 1000
credit_instalments.value = 12

calculateBtn.onclick = Calculate
credit_amount.addEventListener('input', Calculate)
credit_instalments.addEventListener('input', Calculate)

// https://pl.wikipedia.org/wiki/Raty_r%C3%B3wne

// Odsetki na rok
const interest = 0.1

Calculate()

function Calculate() {
  const total_amount = parseInt(credit_amount.value)
  const instalments = parseInt(credit_instalments.value)

  installmentsOutput.innerText = instalments

  // platnosci na rok
  let k = 12
  let N = total_amount
  let r = interest
  let n = instalments


  // Wysokosc raty glownej
  let installmentAmount = (N * r) / (k * (1 - Math.pow(k / k + r), n))

  amountOutput.innerText = installmentAmount.toFixed(2) + ' PLN'
  interestOutput.innerText = (interest * 100) + ' %'
  totalOutput.innerText = (interest * total_amount) + ' PLN'

  let amount_left = total_amount;

  for (let i = 0; i < instalments; i++) {
    const interest_installment = interest / 12 * total_amount
    const installment = total_amount / instalments + interest_installment
    // console.log(installment, interest_installment)

    // renderInstallment()
  }
  // console.log(harmonogram)
}
