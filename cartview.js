
class View {
  constructor(el) {
    this.el = el instanceof HTMLElement ? el : document.querySelector(el)
  }
}

class CartItemView extends View {
  constructor(el, model) {
    super(el)
    this.model = model

    model.addListener(() => {
      this.render()
    })

    this.render()

    this.el.querySelector('.js-close').addEventListener('click', () => {
      cart.remove(model)
    })
  }

  render() {
    const binds = this.el.querySelectorAll(['[data-bind]'])
    binds.forEach(elem => {
      elem.innerText = _.get(this.model, elem.dataset.bind)
    });
  }
}


class CartView extends View {


  constructor(el, cart) {
    super(el)

    this._listEl = this.el.querySelector('#cart-products')
    this._totalEl = this.el.querySelector('#cart-total')

    cart.addListener((type, target) => {
      switch (type) {
        case 'added':
          this.added(target);
          break;
        case 'removed':
          this.removed(target);
          break;
        case 'total':
          this.total(target);
          break;
      }
    })
  }

  added(target) {
    const itemEL = cartItemTpl.content.cloneNode(true).firstElementChild
    this._listEl.append(itemEL)

    this._rendered[target.id] = new CartItemView(itemEL, target)
  }

  _rendered = {}

  removed(target) {
    const el = this._rendered[target.id].el
    this._listEl.children
  }

  total(total) {

  }
}