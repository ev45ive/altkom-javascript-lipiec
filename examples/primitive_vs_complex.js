console.log('Hello Ecommerce app')

// Product data

/*
  - Name
  - Price
  - Promotion
  - Description
*/

var name = 'Placki "Choco\'s" '
var description = "Czekoladowe '100g' "
var price = 10.99
var promotion = true

// Przypisanie przez kopie
name.toLocaleUpperCase() // name sie nie zmienia
name = name.toLocaleUpperCase() // name jest kopia z duzymi literami

var name2 = name; // Kopia
name2.toLowerCase() // Kopia

var product1 = {
  id: '1',
  // name: name.toLocaleUpperCase(),
  name: name,
  description: description,
  price: price.toFixed('2'),
  promotion: promotion,
}

console.log(name + ' ' + price);
console.log(product1.name + ' ' + product1.price);


var product3 = {
  id: '3',
  name: 'Placki "Banana\'s" ',
  description: "Bananowe '100g' ",
  price: 12.99,
  promotion: true,
  review: { rating: 3 }
}

// Przypisanie przez referencje - nadpisujemy product1
var product2 = product1
product2.id = '2'
product2.name = 'Changed name'

// var product4 = {
//   name: product3.name
// }
// product4.name = product3.name
// var product4 = Object.assign({}, product3)
// product4.review = Object.assign({}, product3.review) //, { rating: 5 })
// var product4 = Object.assign({}, product3, { review: Object.assign({}, product3.review) })
var product4 = { ...product3, review: { ...product3.review } }

product4.price = 9.99
product4['price'] = 9.99
// product4[ someKey ] = 9.99

product4.review.rating = 5

obj1 = {a:1,b:2,c:3}
obj2 = {a:'a',  c:'c', d:4}

// obj3 =  Object.assign({} ,obj1, obj2)
obj3 = {  ...obj1, ...obj2  }


console.log(product3.name + ' ' + product3.price + ' rating : ' + product3.review.rating);
console.log(product4.name + ' ' + product4.price + ' rating : ' + product4.review.rating);
