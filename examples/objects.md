```js
function makePerson(name){
    let obj = {}
    obj.name = name
    obj.sayHello = function(){
        return 'Hi, i am ' + obj.name
    }
    return obj
}

alice = makePerson("Alice")
bob = makePerson("Bob")

{name: "Bob", sayHello: ƒ}
alice.sayHello()
"Hi, i am Alice"
bob.sayHello()
"Hi, i am Bob"
```

## Constructors

```js
function makePerson(name){
    this.name = name
    this.sayHello = function(){
        return 'Hi, i am ' + this.name
    }
}

function logger(){ this.log = console.log }

let alice = {}
makePerson.call(alice, 'Alice')
logger.call(alice)

let bob = {}
makePerson.apply(bob, ['Bob'])

alice.sayHello()
"Hi, i am Alice"
alice.log('test')
```

## new keyword
```js
function Person(name){
    this.name = name
    this.sayHello = function(){
        return 'Hi, i am ' + this.name
    }
}

function logger(){ this.log = console.log }


alice = new Person('Alice')
logger.apply(alice)

// let alice = {}
// makePerson.call(alice, 'Alice')


alice.sayHello()
// "Hi, i am Alice"
alice.log('test')
// VM7380:1 test
```

## Prototype constructor

```js
alice = new Person('Alice') 

Person {name: "Alice", sayHello: ƒ}
  name: "Alice"
  sayHello: ƒ ()
  [[Prototype]]: Object
    constructor: ƒ Person(name)

alice instanceof Object
true

alice instanceof Person
true

```

## Prottoype

```js
function Person(name){
    this.name = name

    this.sayHello = sayHello
}

function sayHello(){
    return 'Hi, i am ' + this.name
}

alice = new Person('alice')
bob = new Person('bob')
// Person {name: "bob", sayHello: ƒ}
alice.sayHello == bob.sayHello
// true

```

```js
function Person(name){
    this.name = name
}
Person.prototype.sayHello = function (){
    return 'Hi, i am ' + this.name
}

alice = new Person('alice')
bob = new Person('bob')
alice.sayHello == bob.sayHello
// true
alice 
// Person {name: "alice"}
//   name: "alice"
//   [[Prototype]]: Object
//     sayHello: ƒ ()
//     constructor: ƒ Person(name)
//       [[Prototype]]: Object

bob.sayHello = function(){ return ' I dont talk to you!' }

alice.sayHello()
"Hi, i am alice"

bob.sayHello()
" I dont talk to you!"

bob
// Person {name: "bob", sayHello: ƒ}
//   name: "bob"
//   sayHello: ƒ ()
//   [[Prototype]]: Object
//     sayHello: ƒ ()
//     constructor: ƒ Person(name)

```

## Inheritance 

```js

function Person(name){
    this.name = name
}
Person.prototype.sayHello = function (){
    return 'Hi, i am ' + this.name
}


function Employee(name, salary){
    // extends Person constructor / super()
    Person.apply(this, arguments)
    this.salary = salary 
}
// Employee.prototype = Person.prototype
Employee.prototype = Object.create(Person.prototype)


Employee.prototype.sayHello = function (){
    return Person.prototype.sayHello.call(this) + ' and I make ' + this.salary
}

Employee.prototype.work = function(){
    return ' I would like my ' + this.salary
}

alice = new Person('ALice')
tom = new Employee('Tom',1200)
tom.sayHello()
"Hi, i am Tom and I make 1200"

tom
// Employee {name: "Tom", salary: 1200}
//   name: "Tom"
//   salary: 1200
//   [[Prototype]]: Person
//     sayHello: ƒ ()
//     work: ƒ ()
//       [[Prototype]]: Object
//         sayHello: ƒ ()
//         constructor: ƒ Person(name)

tom instanceof Employee
true

tom instanceof Person 
true


```

## ES2015

```js
class Person{
    constructor(name){
        this.name = name
    }

    sayHello(){  return 'Hi, i am ' + this.name } 
}

class Employee extends Person{
    constructor(name, salary = 0){
        super(name)
        this.salary = salary
    }

    sayHello(){  return super.sayHello() + ' and I make ' + this.salary }
    work(){ return ' I would like my ' + this.salary }
}

alice = new Person('Alice')
tom = new Employee('Tom',2500)

// Employee {name: "Tom", salary: 2500}
//   name: "Tom"
//   salary: 2500
//     [[Prototype]]: Person
//       constructor: class Employee
//       sayHello: ƒ sayHello()
//       work: ƒ work()
//       [[Prototype]]: Object
//         constructor: class Person
//         sayHello: ƒ sayHello()

tom instanceof Employee
true

tom instanceof Person 
true

```

```ts

function Mixin(dynamicClassTypePlacki){
    return class extends dynamicClassTypePlacki{}
}

class Person extends Mixin(Person){

}

```