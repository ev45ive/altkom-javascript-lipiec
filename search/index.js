searchBtn.addEventListener('click', event => {
    productsListEl.innerHTML = ''
    search(searchInput.value)

})

function search(query) {

    fetch('products.json' /* ?query=query */)
        .then(resp => resp.json())
        .then(products => {
            // products.forEach(renderProduct)

            products = products.filter(p => p.name.includes(query))

            items = products.map(renderProduct)
            productsListEl.append(...items)
        })
}


function renderProduct(product) {

    const tpl = productTpl.content.cloneNode(true)

    tpl.querySelector('.js-product-name').innerText = product.name
    tpl.querySelector('.js-product-price').innerText = product.price
    tpl.querySelector('.js-product-description').innerText = product.description || ''
    tpl.querySelector('.js-product-category').innerText = product.category

    // productsListEl.append(tpl)
    return tpl
}