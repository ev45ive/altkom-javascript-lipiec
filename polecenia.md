## GIT

cd ..
git clone https://bitbucket.org/ev45ive/altkom-javascript-lipiec.git altkom-javascript-lipiec
cd altkom-javascript-lipiec

## Git update
git stash -u
git pull

## Instalacje
node -v
v14.15.4

npm -v
6.14.10

git --version
version

code -v
1.40.1

chrome://version 
Google Chrome	92.0

## maszyny 
Imię i Nazwisko: Filip Wrzesiński
Telefon kontaktowy: +48 883 375 186
Email: Filip.Wrzesinski@altkom.pl



## dane 
https://jsonplaceholder.typicode.com/users
https://jsonplaceholder.typicode.com/todos

## Npm
npm init -y 
npm i -D http-server
npm i bootstrap lodash

npx http-server
// lub
npm run start


npm install
npm i 

## json server 
json-server https://jsonplaceholder.typicode.com/db


## Libraries tools frameworks

https://api.jquery.com/on/

https://backbonejs.org/#View

https://lodash.com/

http://rivetsjs.com/docs/guide/ 

https://alpinejs.dev/

https://vuejs.org/v2/guide/


