```js
function calculate(value){
    if(typeof value !== 'number') throw new Error('Expected number type')

    return (value + 10).toFixed(2)
}
// VM6345:2 Uncaught Error: Expected number type
//     at calculate (eval at calculate (index.html:6), <anonymous>:2:41)
//     at eval (eval at calculate (index.html:6), <anonymous>:11:10)
//     at calculate (<anonymous>:6:5)
//     at <anonymous>:11:10
// calculate @ VM6345:2
// eval @ VM6345:11
// calculate @ VM6127:6
// (anonymous) @ VM6127:11

function calculate(value){
    if(typeof value !== 'number') throw new Error('Expected number type')

    try{
        return (value + 10).toFixed(2)
    }catch(error){
        return '0.00'
    }
}

try{
    result = calculate('10')
    console.log(result)
}catch(error){
    console.log('Unexpected error')
}finally{
    console.log('Finished')
}
VM6509:15 Unexpected error
VM6509:17 Finished


```

```js

class MySpecialError extends Error{
    message = 'Special Error'
}


throw new MySpecialError()
// VM7024:1 Uncaught Error: Special Error
//     at <anonymous>:1:7
// MySpecialError @ VM6981:1
// (anonymous) @ VM7024:1

try{
    throw new MySpecialError()
}catch(err){
    if(err instanceof MySpecialError){
        console.log('Special situation')
    }

}
//  Special situation
```