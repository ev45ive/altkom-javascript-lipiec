
(function testFindCartItem() {
  loadCartItems([{
    id: '123',
  }, {
    id: '234',
  }])

  const elem123 = findCartItem('123')
  console.assert(elem123.id === '123', 'Item 123 not found')
  const elem234 = findCartItem('234')
  console.assert(elem234.id === '234', 'Item 234 not found')

})();

(function testAddExistingToCart() {
  // Given
  product = {
    id: '123', price: 100
  }

  loadCartItems([{
    id: '123',
    amount: 2,
    total: 200,
    product: product
  }])

  // When
  addToCart(product)

  // Then
  console.assert(findCartItem('123').id == '123')
  console.assert(findCartItem('123').amount == '3', 'Amount not increased')
  console.assert(findCartItem('123').total == '300', 'Total not increased')
})();

(function testAddNewToCart() {
  // Given
  product = {
    id: '234', price: 100
  }

  loadCartItems([{
    id: '123',
    amount: 2,
    total: 200,
    product: product
  }])


  // When
  addToCart(product)

  // Then
  console.assert(findCartItem('123').id == '123')
  console.assert(findCartItem('123').amount == '2', 'Amount increased')
  console.assert(findCartItem('123').total == '200', 'Total increased')
  console.assert(findCartItem('234'), 'Item not added')
  console.assert(findCartItem('234').id == '234', 'Wrong id')
  console.assert(findCartItem('234').amount == '1', 'Amount increased')
  console.assert(findCartItem('234').total == '100', 'Total increased')
})();


(function testRemoveLastFromCart() {
  // Arrange / Given ...
  product = {
    id: '123', price: 100
  }

  loadCartItems([])
  addToCart(product)

  // Act / When ...
  removeFromCart(product)

  // Assert / Then ...
  console.assert(findCartItem('123') === undefined, 'element wasn removed')
  console.assert(getCartItems().length === 0, 'Cart wasnt emptied')

})();


(function testRemoveOneFromCart() {
  // Arrange / Given ...
  product123 = { id: '123', price: 100 }
  product234 = { id: '234', price: 200 }

  loadCartItems([])
  addToCart(product234)
  addToCart(product123)
  addToCart(product123)

  // Act / When ...
  removeFromCart(product123)

  // Assert / Then ...
  console.assert(findCartItem('234').amount == '1', 'Wrong product')
  console.assert(findCartItem('234').total == '200', 'Wrong product')

  console.assert(findCartItem('123').id == '123')
  console.assert(findCartItem('123').amount == '1', 'Amount not decreased')
  console.assert(findCartItem('123').total == '100', 'Total not decreased')

})();


console.log('Success');