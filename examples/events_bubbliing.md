```html
<div id="container">
  <div class="list-group">
    <div id="child" class="list-group-item">
      <!-- Click here -->
    </div>
  </div>
</div>
```

```js
child.onclick = (event) => {
  event.target === child;
  event.currentTarget === child;
  event.currentTarget === event.target;
};

container.onclick = (event) => {
  event.target === child;
  event.currentTarget === container;
  event.currentTarget !== event.target;
};
```

## Finding ID

```js
$0.getAttribute("data-user-id");
// "1"
$0.dataset;
// DOMStringMap {userId: "1"}
$0.dataset.userId;
// "1"
```

## element maches CSS selector

```js
event.target.matches("[data-user-id]") === true

```

## find parent matching css selector

```js
p = event.target;
while (p && !p.matches("[data-user-id]")) p = p.parentElement;
p;

// <a data-user-id=​"2" href=​"#" class=​"list-group-item list-group-item-action">​…​</a>​

///
event.target.closest("[data-user-id]");
```
