```js
xhr = new XMLHttpRequest()
// xhr.onerror = console.error
xhr.onloadend = console.log 
xhr.open('GET','products.json' /* async = true */)
xhr.send(/* body */)
console.log(xhr.response)

// index.html:1 Access to XMLHttpRequest at 'file:///C:/Projects/szkolenia/altkom-javascript-lipiec/search/products.json' from origin 'null' has been blocked by CORS policy: Cross origin requests are only supported for protocol schemes: http, data, chrome, chrome-extension, chrome-untrusted, https.

```

```js

function getJSON(url){
    var xhr = new XMLHttpRequest()
    // xhr.onerror = console.error
    xhr.onloadend = event => {
        event.target // xhr
        try{
            var result = JSON.parse(xhr.response)
            console.log(result)
        }catch(err){
            throw new Error('Unexpected server response type. Expected JSON')
        }
    },
    xhr.open('GET',url /* async = true */)
    xhr.send(/* body */)
//     console.log(xhr.response)
}

var result = getJSON('products.json')
```