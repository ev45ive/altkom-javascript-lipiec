
/* Model */
let users = []
let tasks = []
let selectedUser = null

// const api_url = 'https://jsonplaceholder.typicode.com/'
const api_url = 'http://localhost:3000/'

const usersListEl = document.querySelector('#usersListEl')
const userDetailsEl = document.querySelector('#userDetailsEl')
const userTasksEl = document.querySelector('#userTasksEl')
const searchInput = document.querySelector('#searchInput')
const searchBtn = document.querySelector('#searchBtn')

searchBtn.addEventListener('click', event => {

    fetchUsers(searchInput.value)
        .then(renderUsers)
})


const p = fetchUsers()
    .then(renderUsers)
p.then(() => checkURLUpdateUser())

window.onpopstate = checkURLUpdateUser

function checkURLUpdateUser() {
    const id = new URLSearchParams(location.search).get('id')
    if (id && id !== selectedUser?.id) {
        selectUserById(id)
    }
}

async function fetchUsers(query) {
    try {
        loaderEl.classList.toggle('d-none', false)
        const params = new URLSearchParams()
        if (query) params.append('q', query)
        
        const res = await fetch(`${api_url}users?` + params)
        const data = await res.json()
        users = data
        return data
    } catch (error) {
        console.log(error)
    } finally {
        loaderEl.classList.toggle('d-none', true)
    }
}

async function getUserTasks(id) {
    const res = await fetch(`${api_url}users/${id}/todos?_limit=10`)
    const data = await res.json()
    tasks = data

    return data
}

async function getUserById(id) {
    // Create resolved promise from cached respose
    const user = users.find(u => u.id == id)
    if (user) return Promise.resolve(user)

    const res = await fetch(`${api_url}users/${id}`)
    return res.json()
}

function renderUsers(users) {
    usersListEl.innerHTML = ''
    users.forEach(user => {
        usersListEl.innerHTML += /* html */`<a data-user-id="${user.id}" href="#" class="list-group-item list-group-item-action">
           <span class="pretty-text"> ${user.name} </span>
        </a>`
    })
}



function renderUserDetails(user) {
    userDetailsEl.innerHTML = /* html */`<dl>
            <dt>Name:</dt>
            <dd>${user.name}</dd>
            <dt>E-mail:</dt>
            <dd>${user.email}</dd>
        </dl>`
}

async function renderUserTasks(userId) {

    userTasksEl.innerHTML = ' '
    // getUserTasks(userId).then(tasks => {
    const tasks = await getUserTasks(userId);

    tasks.forEach(task => {
        userTasksEl.innerHTML += /* html */`<a href="#" data-task-id="${task.id}" class="list-group-item list-group-item-action">
                <span><input type="checkbox" ${task.completed ? 'checked' : ''}></span>
                <span>${task.title}</span>
            </a>`
    })
}

/* Events */

usersListEl.addEventListener('click', event => {

    const userEl = event.target.closest('[data-user-id]')
    const selectedUserId = userEl.dataset.userId

    window.history.pushState('', '', '?id=' + selectedUserId)
})

userTasksEl.addEventListener('change', event => {
    const completed = event.target.checked;
    const taskEl = event.target.closest('[data-task-id]');
    const taskId = taskEl.dataset.taskId
    const task = tasks.find(t => t.id == taskId)
    task.completed = completed
    updateTask(task)
})

function selectUserById(selectedUserId) {
    getUserById(selectedUserId).then(user => {
        selectedUser = user;
        renderUserDetails(user)
    })
    renderUserTasks(selectedUserId)

    const usersListItems = document.querySelectorAll('#usersListEl .list-group-item')
    usersListItems.forEach(item => {
        item.classList.toggle('active', item.dataset.userId === selectedUserId ? true : false)
    })
}

function updateTask(task) {
    return fetch(`${api_url}todos/${task.id}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(task)
    })
}