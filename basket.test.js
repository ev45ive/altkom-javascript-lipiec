

(function testMakeCart() {

  const cart = new Cart()

  cart.add({
    id: '123', price: '100'
  })

  console.assert(cart.getItems()[0].amount == '1')
  console.assert(cart.getItems()[0].total == '100')

})();

(function testCartTotal() {

  const cart = new Cart()
  cart._tax = 0
  console.assert(cart.getTotal() == '0')
  cart.add({ id: '123', price: '100' })
  console.assert(cart.getTotal() == '100')
  cart.add({ id: '123', price: '100' })
  console.assert(cart.getTotal() == '200')
  cart.remove({ id: '123', price: '100' })
  console.assert(cart.getTotal() == '100')

})();


console.log('Success');
