```js

function echo(msg, successCb){
    setTimeout(()=>{
        successCb(msg)
    },1000)
}

echo('Ala', res => {
    echo(res + ' ma ', res => {
        echo(res + ' kota ', res => {
            console.log(res) 
        })
    })
})

```

## Promise

```js

function echo(msg){

    return new Promise((resolve)=>{

        setTimeout(()=>{
            resolve(msg)
        },1000)

    })
}

// Create promise <pending>
promise = echo('Ala')

// Wait for <fullfiled> (resolved) and then ...
promise.then(res => {
    console.log(res) 
})

// Many recipients
promise.then(res => {
    console.log(res) 
})

// Promise "remembers" resolved value
setTimeout(()=>{
    promise.then(res => {
        console.log(res) 
    })
},2000)

```

## Promise chaining / splitting

```js
promise = echo('Ala')

// Chain promises
p2 = promise.then(res => {
    return res + ' ma kota'
})

p2.then(res => {
    console.log(res) 
})

// Split chain
p3 = promise.then(res => {
    return res + ' ma psa'
})

p3.then(res => {
    console.log(res) 
})

//  Ala ma kota
//  Ala ma psa
```

## Nesting promises

```js
promise = echo('Ala')

p2 = promise.then(res => {
    return echo(res + ' ma kota')
//             .then(res => {
//                 console.log(res)
//             })

})

p2.then(res => {
    console.log(res) 
})

```

## nested promise chain

```js
function echo(msg){

    return new Promise((resolve)=>{

        setTimeout(()=>{
            resolve(msg)
        },1000)

    })
}

p = echo('Ala')
    .then(res => echo(res + ' ma '))
    .then(res =>  echo(res + ' kota'));
    
p.then(res => {
    console.log(res) 
})
```

## Error handling

```js

function echo(msg, err){
    return new Promise((resolve, reject)=>{

        setTimeout(()=>{
            err? reject(err) : resolve(msg)
        },1000)

    })
}

p = echo('Ala', 'ups..')
    .then(res => echo(res + ' ma ') , err => 'Nikt nie ma ' )
    .then(res =>  echo(res + ' kota','ups2..'))
    .then(res =>  echo(res + ' i rybki'))
    .catch(err => 'Nie ma zwierzat')

p.then(res => {
    console.log(res) 
})
.finally(() => console.log('done') )

// Promise {<pending>}
//  Nie ma zwierzat
//  done
```

## Not only http
```js
searchBtn = document.createElement('button')

p = new Promise((resolve) => {
    searchBtn.onclick = resolve 
})

p.then(console.log) 

searchBtn.click()
// PointerEvent {isTrusted: false, pointerId: 0, width: 1, height: 1, pressure: 0, …}
// Only once!

// Forever unless removeEventListener
searchBtn.addEventListener('click', console.log)

```

```js

function delay(ms = 1000){
    return new Promise(resolve => setTimeout(resolve,ms) )
}

delay(1000).then(console.log)

```


## XHR Promise
```js
function getJSON(url){
    return new Promise((resolve, reject) => {

        var xhr = new XMLHttpRequest()
        xhr.open('GET',url)

        xhr.onerror = reject
        xhr.onloadend = event => {
            try{
                var result = JSON.parse(xhr.response)
                resolve( result ) 
            }catch(err){
                reject(err)
            }
        },
        xhr.send()
    })
}

var result = getJSON('products.json')
result.then(console.log) 

// (3) [{…}, {…}, {…}]
```

## Fetch API

```js
fetch('products.json')
    .then( resp => {
        return resp.json().then( resp => {
            console.log(resp) 
        })
    }) 

// Promise {<pending>}
// VM7248:4 (3) [{…}, {…}, {…}]
// VM6730:1 Fetch finished loading: GET "http://localhost:9000/search/products.json".

fetch('products.json')
.then( resp => resp.json()) 
.then( resp => {
    console.log(resp) 
})
// Promise {<pending>}
// VM6730:1 Fetch finished loading: GET "http://localhost:9000/search/products.json".
// VM7295:4 (3) [{…}, {…}, {…}]
```