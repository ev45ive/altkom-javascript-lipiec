```js
function calculate(value){
    return value * 10
}

result = calculate(10)

console.log(result)


```

```js

function calculate(value){
    setTimeout(()=>{
        result = value * 10
        renderResult(result)
    },1000)
}

function renderResult(result){
    console.log(result)
}

result = calculate(10)

// 100
btn = document.createElement('button')
btn.onclick = console.log
btn.click()
// PointerEvent {isTrusted: false, pointerId: 0, width: 1, height: 1, pressure: 0, …}

function calculate(value, callback){
    setTimeout(()=>{
        result = value * 10
        callback(result)
    },1000)
}

function renderResult(result){
    console.log(result)
}

// result = calculate(10)
calculate(10, renderResult)

// 100
```

## error handling

```js
function calculate(value, callback){
    setTimeout(()=>{
        result = (value + 10).toFixed(2)
        callback(result)
    },1000)
}
​
function renderResult(result){
    console.log(result)
}
​
try{
    calculate('ala', renderResult)
}catch(err){
    console.log('ups..')
}

```

```js
function calculate(value, callback /*, errBack */){
    setTimeout(()=>{
        try{
            result = (value + 10).toFixed(2)
            callback(null,result)
//             callback(result)
        }catch(err){
            callback(err)
            // errBack(err)
        }
    },1000)
}
​
function renderResult(err,result){
    if(err){ return console.log('Unexpected error') }
​
    console.log(result)
}
​
calculate('ala', renderResult)

```