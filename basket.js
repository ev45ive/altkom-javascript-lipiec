class Observer {

  _listeners = []
  addListener(ln) { this._listeners.push(ln) }

  notify(type = 'change', param = this) {
    this._listeners.forEach(fn => fn(type, param))
  }
}
class CartItem extends Observer {
  constructor(product, amount = 1) {
    super()
    this.id = product.id
    this.product = product
    this.amount = amount
    this._calculate()
  }


  increase(amount = 1) {
    this.amount += amount
    this._calculate()
  }

  decrease(amount = 1) {
    this.amount -= amount
    this._calculate()
  }

  _calculate() {
    this.total = this.product.price * this.amount
    this.notify()
  }
}

class Cart extends Observer {
  _items = []
  _total = 0
  _tax = 0.1

  constructor() {
    super()
  }

  _recalculate() {
    this._total = this._items.reduce(
      (total, item) => total + (item.total * (1 - this._tax)), 0)
    this.notify('total', this._total)
  }

  getItems() {
    return this._items
  }

  getTotal() {
    return this._total
  }

  loadItems(items) {
    this._items = items.map(item => new CartItem(item.product, item.amount))
    this._recalculate()
  }

  add(product) {
    const item = this.get(product.id)

    if (item) {
      item.increase()
    } else {
      const item = new CartItem(product)
      this._items.push(item)
      this.notify('added', item)
    }
    this._recalculate()
  }

  remove(product) {
    const item = this.get(product.id)
    if (!item) return

    if (item.amount <= 1) {
      this._items = this._items.filter(i => i.id !== item.id)
      this.notify('removed', item)
    } else {
      item.decrease()
    }
    this._recalculate()
  }

  get(id) {
    return this._items.find(item => item.id === id)
  }

}