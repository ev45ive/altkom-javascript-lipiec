```js

var x = 1

setTimeout(() => x = 2, 0)

console.log(x) 
// 1
console.log(x) 
// 2

```

```js
searchEl.addEventListener('input', event => {
    console.log(event.target.value)
})

console.log(1)

setTimeout( () => console.log(2), 0)

Promise.resolve(3).then(console.log) 

console.log(4)

start = Date.now()

while( start + 5 * 1000 > Date.now()) { }

console.log(5)
1
4
5
3
undefined
a
al
ala
ala 
ala m
ala ma
ala ma 
ala ma k
ala ma ko
ala ma kot
ala ma kota
2
```

## TIme dilation ;-)

```js

var i = 0
var start = Date.now()

var handler = setInterval(()=>{
    i += 1000;

    if(start + 3000 < Date.now() ){
        console.log('alert!', i, Date.now() - start) 
        clearInterval(handler)
    }
},1000)
// When tab is active:
alert! 3000 3012

// When tab is inactive:
alert! 5000 5151
```